#!/usr/bin/python3
import requests
import math
import json 

email_response = requests.get('https://paystand.ml?email=sergiomen4@gmail.com')
email_response = email_response.json()
token = email_response["token"]
age_avg = []
phrase = []
names = []
status_code = 200
data = []

f = open("token.txt", "w")
f.write(token)
f.close()

#loop until get 404 while get the age and name of each person
while status_code == 200:
		token_response = requests.get('https://paystand.ml/data?token=' + str(token))
		json_response = token_response.json()
		status_code = int(token_response.status_code)
		if status_code != 200:
			break
		phrase.append(json_response["name"])
		age_avg.append(json_response["age"])
		data.append(json_response)
		print(json_response)

def get_phrase(a,b):
	p = []
	w = sorted(phrase[a:b+1])
	for i in w:
		p.append(i.split(" ")[1][0])
	return "".join(p)

def longest_increasing_subsequence(arr):
    s, f = 0, 0
    S, F = 0, 0
    currentMaxLen = 0
    for i in range(len(arr) - 1):
        f += 1
        if arr[i] > arr[i+1]:
            s = i + 1
        if (f-s) > currentMaxLen:
            S, F = s, f
            currentMaxLen = f-s
    return S, F

def get_avg():
	a,b = longest_increasing_subsequence(age_avg)
	return math.trunc(sum(age_avg[a:b+1]) / len(age_avg[a:b+1])), get_phrase(a,b)

j = {
	"age": get_avg()[0],
	"payload": get_avg()[1],
	"code": "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4"
}

j = json.dumps(j)
f = open("file.json", "w")
f.write(j)
f.close()



